#!/bin/bash -ex

source ./build-common.sh

# BUILDDIR_NATIVE=/home/arturo/Scaricati/ARM_WORK/gcc-arm-none-eabi-4_8-2014q1-20140314/build-native
# INSTALLDIR_NATIVE=/home/arturo/Scaricati/ARM_WORK/gcc-arm-none-eabi-4_8-2014q1-20140314/install-native

export ARCH=arm CROSS_COMPILE=armv7-rpi2-linux-gnueabihf-
export PATH=/opt/armv7-rpi2-linux-gnueabihf/bin:/opt/gcc-arm-none-eabi-4.8.3-2014q1/bin:$PATH

copy_multi_libs src_prefix="$BUILDDIR_NATIVE/target-libs/arm-none-eabi/lib" \
                dst_prefix="$INSTALLDIR_NATIVE/arm-none-eabi/lib"           \
                target_gcc="arm-none-eabi-gcc"
