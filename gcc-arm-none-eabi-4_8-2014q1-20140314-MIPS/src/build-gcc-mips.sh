#!/bin/bash

export ARCH=mips CROSS_COMPILE=mips-openwrt-linux-uclibc-
export PATH=/opt/toolchain-48-1505/bin:$PATH
export STAGING_DIR=/opt/toolchain-48-1505
LIBPATH=/opt/toolchain-48/lib/
export LDFLAGS='-Wl,-rpath-link '${LIBPATH}

# TARGET_LANGUAGES="c,c++"
# BUGURL="https://dev.openwrt.org/"
# PKGVERSION="OpenWrt GCC 4.8.3"

# (cd /home/arturo/Scaricati/openwrt-15.05/build_dir/target-mips_34kc_uClibc-0.9.33.2/gcc-4.8.3; \
# rm -f config.cache; SHELL="bash" AR="mips-openwrt-linux-uclibc-gcc-ar" \
# AS="mips-openwrt-linux-uclibc-gcc -c -Os -pipe -mno-branch-likely -mips32r2 -mtune=34kc -fno-caller-saves -fhonour-copts -Wno-error=unused-but-set-variable -Wno-error=unused-result -msoft-float" \
# LD=mips-openwrt-linux-uclibc-ld NM="mips-openwrt-linux-uclibc-gcc-nm"
# CC="mips-openwrt-linux-uclibc-gcc" GCC="mips-openwrt-linux-uclibc-gcc"
# CXX="mips-openwrt-linux-uclibc-g++" RANLIB="mips-openwrt-linux-uclibc-gcc-ranlib"
# STRIP=mips-openwrt-linux-uclibc-strip OBJCOPY=mips-openwrt-linux-uclibc-objcopy
# OBJDUMP=mips-openwrt-linux-uclibc-objdump SIZE=mips-openwrt-linux-uclibc-size
#
# /home/arturo/Scaricati/openwrt-15.05/build_dir/target-mips_34kc_uClibc-0.9.33.2/gcc-4.8.3/configure \
#     --target=mips-openwrt-linux --host=mips-openwrt-linux --build=i686-linux-gnu \
#     --program-prefix="" --program-suffix="" --prefix=/usr --exec-prefix=/usr --bindir=/usr/bin \
#     --sbindir=/usr/sbin --libexecdir=/usr/lib --sysconfdir=/etc --datadir=/usr/share \
#     --localstatedir=/var --mandir=/usr/man --infodir=/usr/info --disable-nls   \
#     --build=i686-linux-gnu --host=mips-openwrt-linux-uclibc --target=mips-openwrt-linux-uclibc \
#     --enable-languages="c,c++" --with-bugurl=https://dev.openwrt.org/ --with-pkgversion="OpenWrt GCC 4.8.3" \
#     --enable-shared --disable-__cxa_atexit --enable-target-optspace --with-gnu-ld --disable-nls \
#     --disable-libmudflap --disable-multilib --disable-libgomp --disable-libquadmath --disable-libssp \
#     --disable-decimal-float --disable-libstdcxx-pch --with-host-libstdcxx=-lstdc++ --prefix=/usr \
#     --libexecdir=/usr/lib --with-float=soft   );

cd gcc-build/

../gcc/configure \
    --build=i686-linux-gnu \
    --host=mips-openwrt-linux-uclibc \
    --enable-languages=c,c++ \
    --with-pkgversion="OpenWrt GCC 4.8.3" \
    --enable-shared \
    --disable-__cxa_atexit \
    --enable-target-optspace \
    --with-gnu-ld \
    --disable-nls \
    --disable-libmudflap \
    --disable-multilib \
    --disable-libgomp \
    --disable-libquadmath \
    --disable-libssp \
    --disable-decimal-float \
    --disable-libstdcxx-pch \
    --with-host-libstdcxx=-lstdc++ \
    --prefix=/usr \
    --libexecdir=/usr/lib \
    --with-float=soft

make -j3
