#!/bin/bash -ex

mkdir -p $PWD/src/bin

mkdir -p toolsdir/bin
cd toolsdir
TOOLS_PATH=`pwd`
cd bin
TOOLS_BIN_PATH=`pwd`
cd ../../

export PATH="$TOOLS_BIN_PATH:$PATH"

if [ -z "$MAKE_JOBS" ]; then
	MAKE_JOBS="2"
fi

if [[ ! -f src/autoconf-2.64.tar.bz2  ]] ;
then
	wget http://mirror.switch.ch/ftp/mirror/gnu/autoconf/autoconf-2.64.tar.bz2 -O src/autoconf-2.64.tar.bz2
fi

tar xfjv src/autoconf-2.64.tar.bz2 -C .

cd autoconf-2.64

CONFARGS="--prefix=$TOOLS_PATH"

./configure $CONFARGS

nice -n 10 make -j $MAKE_JOBS

make install

cd -

if [[ ! -f src/automake-1.11.1.tar.bz2  ]] ;
then
	wget http://mirror.switch.ch/ftp/mirror/gnu/automake/automake-1.11.1.tar.bz2 -O src/automake-1.11.1.tar.bz2
fi

tar xfjv src/automake-1.11.1.tar.bz2 -C .

cd automake-1.11.1

./bootstrap

CONFARGS="--prefix=$TOOLS_PATH"

./configure $CONFARGS

nice -n 10 make -j $MAKE_JOBS

make install

cd -

rm -rf autoconf-2.64 automake-1.11.1
