#! /usr/bin/env bash
# Copyright (c) 2011-2013, ARM Limited
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
#     * Redistributions of source code must retain the above copyright notice,
#       this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above copyright
#       notice, this list of conditions and the following disclaimer in the
#       documentation and/or other materials provided with the distribution.
#     * Neither the name of ARM nor the names of its contributors may be used
#       to endorse or promote products derived from this software without
#       specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

set -e
set -x
set -u
set -o pipefail

umask 022

exec < /dev/null

script_path=`cd $(dirname $0) && pwd -P`
. $script_path/build-common-armv8-02.sh

# cross compile exports

export ARCH=arm CROSS_COMPILE=armv8-rpi3-linux-gnueabihf-
export HOST=armv8-rpi3-linux-gnueabihf
export PATH=/opt/armv8-rpi3-linux-gnueabihf/bin:/opt/gcc-arm-none-eabi-4.8.3-2014q1/bin:$PATH

export CPP="${HOST}-gcc -E"
export STRIP="${HOST}-strip"
export OBJCOPY="${HOST}-objcopy"
export AR="${HOST}-ar"
export RANLIB="${HOST}-ranlib"
export LD="${HOST}-g++"
export OBJDUMP="${HOST}-objdump"
export CC="${HOST}-gcc"
export CXX="${HOST}-g++"
export NM="${HOST}-nm"
export AS="${HOST}-as"
export PS1="[${HOST}] \w$ "

# end of cross-compile exports

# This file contains the sequence of commands used to build the ARM EABI toolchain.
usage ()
{
    echo "Usage:" >&2
    echo "      $0 [--skip_mingw32] [--debug] [--ppa] [--skip_manual]" >&2
    exit 1
}
if [ $# -gt 4 ] ; then
    usage
fi
skip_mingw32=yes
DEBUG_BUILD_OPTIONS=
is_ppa_release=no
skip_manual=yes
MULTILIB_LIST="--with-multilib-list=armv6-m,armv7-m,armv7e-m,armv7-r"
for ac_arg; do
    case $ac_arg in
        --skip_mingw32)
            skip_mingw32=yes
            ;;
        --debug)
            DEBUG_BUILD_OPTIONS=" -O0 -g "
            ;;
        --ppa)
            is_ppa_release=yes
            skip_mingw32=yes
            ;;
	--skip_manual)
	    skip_manual=yes
	    ;;
        *)
            usage
            ;;
    esac
done

if [ "x$BUILD" == "xx86_64-apple-darwin10" ]; then
    skip_mingw32=yes
fi

if [ "x$is_ppa_release" != "xyes" ]; then
  ENV_CFLAGS=" -I$BUILDDIR_NATIVE/host-libs/zlib/include -O2 "
  ENV_CPPFLAGS=" -I$BUILDDIR_NATIVE/host-libs/zlib/include "
  ENV_LDFLAGS=" -L$BUILDDIR_NATIVE/host-libs/zlib/lib
                -L$BUILDDIR_NATIVE/host-libs/usr/lib "

  GCC_CONFIG_OPTS=" --build=$BUILD --host=$HOST_NATIVE
                    --with-gmp=$BUILDDIR_NATIVE/host-libs/usr
                    --with-mpfr=$BUILDDIR_NATIVE/host-libs/usr
                    --with-mpc=$BUILDDIR_NATIVE/host-libs/usr
                    --with-isl=$BUILDDIR_NATIVE/host-libs/usr
                    --with-cloog=$BUILDDIR_NATIVE/host-libs/usr
                    --with-libelf=$BUILDDIR_NATIVE/host-libs/usr "

  BINUTILS_CONFIG_OPTS=" --build=$BUILD --host=$HOST_NATIVE "

  NEWLIB_CONFIG_OPTS=" --build=$BUILD --host=$HOST_NATIVE "

  GDB_CONFIG_OPTS=" --build=$BUILD --host=$HOST_NATIVE
                    --with-libexpat-prefix=$BUILDDIR_NATIVE/host-libs/usr "
fi

# prints only env
sleep 2
# exit 0

echo Task [III-9] /$HOST_NATIVE/strip_host_objects/
if [ "x$DEBUG_BUILD_OPTIONS" = "x" ] ; then
    STRIP_BINARIES=`find $INSTALLDIR_NATIVE/bin/ -name arm-none-eabi-\*`
    for bin in $STRIP_BINARIES ; do
        strip_binary $HOST_NATIVE-strip $bin
    done

    STRIP_BINARIES=`find $INSTALLDIR_NATIVE/arm-none-eabi/bin/ -maxdepth 1 -mindepth 1 -name \*`
    for bin in $STRIP_BINARIES ; do
        strip_binary $HOST_NATIVE-strip $bin
    done

    STRIP_BINARIES=`find $INSTALLDIR_NATIVE/lib/gcc/arm-none-eabi/$GCC_VER/ -maxdepth 1 -name \* -perm +111 -and ! -type d`
    for bin in $STRIP_BINARIES ; do
        strip_binary $HOST_NATIVE-strip $bin
    done
fi

echo Task [III-10] /$HOST_NATIVE/strip_target_objects/
saveenv
# prepend_path PATH $INSTALLDIR_NATIVE/bin
TARGET_LIBRARIES=`find $INSTALLDIR_NATIVE/arm-none-eabi/lib -name \*.a`
for target_lib in $TARGET_LIBRARIES ; do
    arm-none-eabi-objcopy -R .comment -R .note -R .debug_info -R .debug_aranges -R .debug_pubnames -R .debug_pubtypes -R .debug_abbrev -R .debug_line -R .debug_str -R .debug_ranges -R .debug_loc $target_lib || true
done

TARGET_OBJECTS=`find $INSTALLDIR_NATIVE/arm-none-eabi/lib -name \*.o`
for target_obj in $TARGET_OBJECTS ; do
    arm-none-eabi-objcopy -R .comment -R .note -R .debug_info -R .debug_aranges -R .debug_pubnames -R .debug_pubtypes -R .debug_abbrev -R .debug_line -R .debug_str -R .debug_ranges -R .debug_loc $target_obj || true
done

TARGET_LIBRARIES=`find $INSTALLDIR_NATIVE/lib/gcc/arm-none-eabi/$GCC_VER -name \*.a`
for target_lib in $TARGET_LIBRARIES ; do
    arm-none-eabi-objcopy -R .comment -R .note -R .debug_info -R .debug_aranges -R .debug_pubnames -R .debug_pubtypes -R .debug_abbrev -R .debug_line -R .debug_str -R .debug_ranges -R .debug_loc $target_lib || true
done

TARGET_OBJECTS=`find $INSTALLDIR_NATIVE/lib/gcc/arm-none-eabi/$GCC_VER -name \*.o`
for target_obj in $TARGET_OBJECTS ; do
    arm-none-eabi-objcopy -R .comment -R .note -R .debug_info -R .debug_aranges -R .debug_pubnames -R .debug_pubtypes -R .debug_abbrev -R .debug_line -R .debug_str -R .debug_ranges -R .debug_loc $target_obj || true
done
restoreenv

# PPA release needn't following steps, so we exit here.
if [ "x$is_ppa_release" == "xyes" ] ; then
  exit 0
fi

echo Task [III-11] /$HOST_NATIVE/package_tbz2/
rm -f $PACKAGEDIR/$PACKAGE_NAME_NATIVE.tar.bz2
pushd $BUILDDIR_NATIVE
rm -f $INSTALL_PACKAGE_NAME
cp $ROOT/$RELEASE_FILE $INSTALLDIR_NATIVE_DOC/
cp $ROOT/$README_FILE $INSTALLDIR_NATIVE_DOC/
cp $ROOT/$LICENSE_FILE $INSTALLDIR_NATIVE_DOC/
copy_dir_clean $SRCDIR/$SAMPLES $INSTALLDIR_NATIVE/share/gcc-arm-none-eabi/$SAMPLES
ln -s $INSTALLDIR_NATIVE $INSTALL_PACKAGE_NAME
${TAR} cjf $PACKAGEDIR/$PACKAGE_NAME_NATIVE.tar.bz2   \
    --owner=0                               \
    --group=0                               \
    --exclude=host-$HOST_NATIVE             \
    --exclude=host-$HOST_MINGW              \
    $INSTALL_PACKAGE_NAME/arm-none-eabi     \
    $INSTALL_PACKAGE_NAME/bin               \
    $INSTALL_PACKAGE_NAME/lib               \
    $INSTALL_PACKAGE_NAME/share
rm -f $INSTALL_PACKAGE_NAME
popd

# skip building mingw32 toolchain if "--skip_mingw32" specified
# this huge if statement controls all $BUILDDIR_MINGW tasks till "task [3-1]"
if [ "x$skip_mingw32" != "xyes" ] ; then
    echo "none"
fi #end of if [ "x$skip_mingw32" != "xyes" ] ;

# Disabling packaging

# echo Task [V-0] /package_sources/
# pushd $PACKAGEDIR
# rm -rf $PACKAGE_NAME && mkdir -p $PACKAGE_NAME/src
# cp -f $SRCDIR/$CLOOG_PACK $PACKAGE_NAME/src/
# cp -f $SRCDIR/$EXPAT_PACK $PACKAGE_NAME/src/
# cp -f $SRCDIR/$GMP_PACK $PACKAGE_NAME/src/
# cp -f $SRCDIR/$LIBELF_PACK $PACKAGE_NAME/src/
# cp -f $SRCDIR/$LIBICONV_PACK $PACKAGE_NAME/src/
# cp -f $SRCDIR/$MPC_PACK $PACKAGE_NAME/src/
# cp -f $SRCDIR/$MPFR_PACK $PACKAGE_NAME/src/
# cp -f $SRCDIR/$ISL_PACK $PACKAGE_NAME/src/
# cp -f $SRCDIR/$ZLIB_PATCH $PACKAGE_NAME/src/
# cp -f $SRCDIR/$ZLIB_PACK $PACKAGE_NAME/src/
# pack_dir_clean $SRCDIR $BINUTILS $PACKAGE_NAME/src/$BINUTILS.tar.bz2
# pack_dir_clean $SRCDIR $GCC $PACKAGE_NAME/src/$GCC.tar.bz2
# pack_dir_clean $SRCDIR $GDB $PACKAGE_NAME/src/$GDB.tar.bz2 \
#   --exclude="gdb/testsuite/config/qemu.exp" --exclude="sim"
# pack_dir_clean $SRCDIR $NEWLIB $PACKAGE_NAME/src/$NEWLIB.tar.bz2
# pack_dir_clean $SRCDIR $NEWLIB_NANO $PACKAGE_NAME/src/$NEWLIB_NANO.tar.bz2
# pack_dir_clean $SRCDIR $SAMPLES $PACKAGE_NAME/src/$SAMPLES.tar.bz2
# pack_dir_clean $SRCDIR $BUILD_MANUAL $PACKAGE_NAME/src/$BUILD_MANUAL.tar.bz2
# if [ -d $SRCDIR/$GCC_PLUGINS/ ]; then
#   pack_dir_clean $SRCDIR $GCC_PLUGINS $PACKAGE_NAME/src/$GCC_PLUGINS.tar.bz2
# fi
# if [ "x$skip_mingw32" != "xyes" ] ; then
#     pack_dir_clean $SRCDIR $INSTALLATION \
#       $PACKAGE_NAME/src/$INSTALLATION.tar.bz2 \
#       --exclude=build.log --exclude=output
# fi
# cp $ROOT/$RELEASE_FILE $PACKAGE_NAME/
# cp $ROOT/$README_FILE $PACKAGE_NAME/
# cp $ROOT/$LICENSE_FILE $PACKAGE_NAME/
# cp $ROOT/$BUILD_MANUAL_FILE $PACKAGE_NAME/
# cp $ROOT/build-common.sh $PACKAGE_NAME/
# cp $ROOT/build-prerequisites.sh $PACKAGE_NAME/
# cp $ROOT/build-toolchain.sh $PACKAGE_NAME/
# tar cjf $PACKAGE_NAME-src.tar.bz2 $PACKAGE_NAME
# rm -rf $PACKAGE_NAME
# popd
#
# echo Task [V-1] /md5_checksum/
# pushd $PACKAGEDIR
# rm -rf md5.txt
# $MD5 $PACKAGE_NAME_NATIVE.tar.bz2     >>md5.txt
# if [ "x$skip_mingw32" != "xyes" ] ; then
#     $MD5 $PACKAGE_NAME_MINGW.exe         >>md5.txt
#     $MD5 $PACKAGE_NAME_MINGW.zip         >>md5.txt
# fi
# $MD5 $PACKAGE_NAME-src.tar.bz2 >>md5.txt
# popd
