#!/bin/bash -ex

source ./build-common-armv6-01.sh

# BUILDDIR_NATIVE=/home/arturo/Scaricati/ARM_WORK/gcc-arm-none-eabi-4_8-2014q1-20140314/build-native

# INSTALL_NATIVEDIR=/home/arturo/Scaricati/ARM_WORK/gcc-arm-none-eabi-4_8-2014q1-20140314/install-native

export ARCH=arm CROSS_COMPILE=armv7-rpi2-linux-gnueabihf-
export PATH=/opt/armv6-rpi-linux-gnueabi/bin:/opt/gcc-arm-none-eabi-4.8.3-2014q1/bin:$PATH

GCC_VER=4.8.3
DEBUG_BUILD_OPTIONS=""

# PPA release needn't following steps, so we exit here.
# if [ "x$is_ppa_release" == "xyes" ] ; then
#   exit 0
# fi

echo Task [III-11] /$HOST_NATIVE/package_tbz2/
rm -f $PACKAGEDIR/$PACKAGE_NAME_NATIVE.tar.bz2
pushd $BUILDDIR_NATIVE
rm -f $INSTALL_PACKAGE_NAME
cp $ROOT/$RELEASE_FILE $INSTALLDIR_NATIVE_DOC/
cp $ROOT/$README_FILE $INSTALLDIR_NATIVE_DOC/
cp $ROOT/$LICENSE_FILE $INSTALLDIR_NATIVE_DOC/
copy_dir_clean $SRCDIR/$SAMPLES $INSTALLDIR_NATIVE/share/gcc-arm-none-eabi/$SAMPLES
ln -s $INSTALLDIR_NATIVE $INSTALL_PACKAGE_NAME
${TAR} cjf $PACKAGEDIR/$PACKAGE_NAME_NATIVE.tar.bz2   \
    --owner=0                               \
    --group=0                               \
    --exclude=host-$HOST_NATIVE             \
    --exclude=host-$HOST_MINGW              \
    $INSTALL_PACKAGE_NAME/arm-none-eabi     \
    $INSTALL_PACKAGE_NAME/bin               \
    $INSTALL_PACKAGE_NAME/lib               \
    $INSTALL_PACKAGE_NAME/share
rm -f $INSTALL_PACKAGE_NAME
popd
