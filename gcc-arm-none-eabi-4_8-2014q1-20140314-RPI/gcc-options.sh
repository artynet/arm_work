GCC 2ND PASS OPTIONS

---------------------------------

MULTILIB_LIST="--with-multilib-list=armv6-m,armv7-m,armv7e-m,armv7-r"

--with-multilib-list=armv6-m,armv7-m,armv7e-m,armv7-r --with-host-libstdcxx="-static-libgcc -Wl,-Bstatic,-lstdc++,-Bdynamic -lm"

INSTALL_NATIVEDIR=/home/arturo/Scaricati/ARM_WORK/gcc-arm-none-eabi-4_8-2014q1-20140314/install-native

plugin_dir=$INSTALL_NATIVEDIR/lib/gcc/arm-none-eabi/4.8.3/plugin

armv7-rpi2-linux-gnueabihf-g++ -fPIC -fno-rtti -O2 -shared \
    -I /home/arturo/Scaricati/ARM_WORK/gcc-arm-none-eabi-4_8-2014q1-20140314/build-native/host-libs/usr/include \
    -I $plugin_dir/include /home/arturo/Scaricati/ARM_WORK/gcc-arm-none-eabi-4_8-2014q1-20140314/src/gcc-plugins/tree_switch_shortcut_elf/tree-switch-shortcut.c \
    -o $plugin_dir/tree_switch_shortcut_elf.so
